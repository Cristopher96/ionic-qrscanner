import { Component } from '@angular/core';
import { DataLocalService } from '../../services/data-local.service';
import { Record } from '../../models/register.model';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  constructor(public dataLocal: DataLocalService) {}

  sendEmail() {
    this.dataLocal.sendEmail();
  }

  openRegister(r: Record) {
    this.dataLocal.openRecord(r);
  }

  clear() {
    this.dataLocal.clearStorage();
  }
}
