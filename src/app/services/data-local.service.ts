import { Injectable } from '@angular/core';
import { Record } from '../models/register.model';
import { Storage } from '@ionic/storage';
import { NavController, ToastController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { File } from '@ionic-native/file/ngx';
import { EmailComposer } from '@ionic-native/email-composer/ngx';

@Injectable({
  providedIn: 'root'
})
export class DataLocalService {

  saved: Record[] = [];
  fileName = 'records.csv';

  constructor(
    private storage: Storage,
    private navCtrl: NavController,
    private iab: InAppBrowser,
    private file: File,
    private emailCmpsr: EmailComposer,
    private toastCtrl: ToastController
  ) {
    this.loadRecords();
  }

  async loadRecords() {
    this.saved = await this.storage.get('records') || [];   
  }

  async saveRegister(format: string, text: string) {
    await this.loadRecords();
    const newRecord = new Record(format, text);
    this.saved.unshift(newRecord);
    this.storage.set('records', this.saved);
    this.openRecord(newRecord);
  }

  openRecord(r: Record) {
    this.navCtrl.navigateForward('/tabs/tab2');
    switch (r.type) {
      case 'http':
        this.iab.create(r.text, '_system');
        break;
      case 'geo':
        this.navCtrl.navigateForward(`/tabs/tab2/map/${r.text}`);
        break;
    }
  }

  sendEmail() {
    const tempArr = [];
    const titles = 'Type, Format, Created At, Text\n';
    tempArr.push(titles);
    this.saved.forEach(record => {
      const line = `${record.type}, ${record.format}, ${record.created}, ${record.text.replace(',', ' ')}\n`;
      tempArr.push(line);
    });
    this.createFile(tempArr.join(''));
    this.presentToast('Email Sended');
  }

  createFile(text: string) {
    this.file.checkFile(this.file.dataDirectory, this.fileName).then(exists => {
      return this.writeFile(text);
    }).catch(err => {
      return this.file.createFile(this.file.dataDirectory, this.fileName, false).then(created => this.writeFile(text)).catch(err2 => {
        this.presentToast(`Couldn't create the file`);
      });
    });
  }

  async writeFile(text: string) {
    await this.file.writeExistingFile(this.file.dataDirectory, this.fileName, text);
    const file = `${this.file.dataDirectory}${this.fileName}`;
    const email = {
      to: 'escorciacristopher@gmail.com',
      attachments: [
        file
      ],
      subject: 'Scans BackUp',
      body: 'Here are the scans backup from <strong>QRScannerApp</strong>',
      isHtml: true
    };
    this.emailCmpsr.open(email);
  }

  async presentToast(m: string) {
    const toast = await this.toastCtrl.create({
      message: m,
      duration: 1500
    });
    toast.present();
  }

  clearStorage() {
    this.storage.clear();
    this.presentToast('All items deleted.');
    this.loadRecords();
  }
}
